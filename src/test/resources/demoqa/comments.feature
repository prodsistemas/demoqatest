Feature: Send comments from the Sample Page

  In order to send a comment from the sample page
  As a user of the application
  I want to be able to send a comment only when email address is correct

  Scenario Outline: Should send a comment only when email address is correct
    Given I am on the sample page
    And I previously tried to send the comment '<comment>', the name '<name>' and with the wrong email '<wrong_email>', But it is shown an error message '<message>'
    When I try again to send the comment '<comment>', the name '<name>' and with the correct email '<correct_email>'
    Then I should post the comment successfully
    Examples:
      | comment                           | name             | wrong_email        | correct_email           | message                             |
      | This comment have been written at | Pedro Rodriguez  | prodsistemas@gmail | prodsistemas@gmail.com  | please enter a valid email address. |