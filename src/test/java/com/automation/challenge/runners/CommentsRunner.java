package com.automation.challenge.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/demoqa/comments.feature",
        format = {"html:target/cucumber-html-report", "json:target/cucumber-json-report.json" },
        glue = {"com.automation.challenge"}
)
public class CommentsRunner {
}
