package com.automation.challenge.builders;

import com.automation.challenge.interfaces.WithEmail;
import com.automation.challenge.interfaces.WithName;
import com.automation.challenge.models.FormSample;

public class FormSamplePageBuilder implements WithName, WithEmail {
    private FormSample formSample;

    public FormSamplePageBuilder(String theComment) {
        this.formSample = new FormSample();
        formSample.comment = theComment;
    }

    public static WithName comment(String theComment) {
        return new FormSamplePageBuilder(theComment);
    }

    public WithEmail name(String theName){
        formSample.name = theName;
        return this;
    }

    public FormSamplePageBuilder email(String theEmail) {
        formSample.email = theEmail;
        return this;
    }

    public FormSamplePageBuilder website(String theWebsite){
        formSample.website = theWebsite;
        return this;
    }

    public FormSample build(){
        return formSample;
    }
}
