package com.automation.challenge.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

@DefaultUrl("http://store.demoqa.com/")
public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

}
