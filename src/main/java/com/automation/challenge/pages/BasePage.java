package com.automation.challenge.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class BasePage extends PageObject {
    @FindBy(id = "menu-footer-menu")
    WebElementFacade footerMenu;

    public BasePage(WebDriver driver) {
        super(driver);
    }

    public void selectFooterMenuOption(String menuItem) {
        footerMenu.findElement(By.linkText(menuItem)).click();
    }
}
