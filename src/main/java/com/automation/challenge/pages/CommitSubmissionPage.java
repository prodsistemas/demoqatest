package com.automation.challenge.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebDriver;

public class CommitSubmissionPage extends BasePage{


    @FindBy(id = "error-page")
    WebElementFacade lblMessage;

    @FindBy(tagName = "a")
    WebElementFacade lnkBack;

    public CommitSubmissionPage(WebDriver driver) {
        super(driver);
    }

    public String verifySubmission() {
        return lblMessage.getText();
    }

    public void goBack() {
        lnkBack.click();
    }
}
