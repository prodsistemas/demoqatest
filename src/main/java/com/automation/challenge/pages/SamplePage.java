package com.automation.challenge.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebDriver;

public class SamplePage extends BasePage{

    @FindBy(id = "comment")
    WebElementFacade txtComment;

    @FindBy(id = "author")
    WebElementFacade txtName;

    @FindBy(id = "email")
    WebElementFacade txtEmail;

    @FindBy(id = "url")
    WebElementFacade txtWebsite;

    @FindBy(id = "submit")
    WebElementFacade btnSubmit;

    @FindBy(id = "comments")
    WebElementFacade cntComments;

    public SamplePage(WebDriver driver) {
        super(driver);
    }

    public void fillComment(String theComment) {
        txtComment.type(theComment);
    }

    public void fillName(String name) {
        txtName.type(name);
    }

    public void fillEmail(String email) {
        txtEmail.type(email);
    }

    public void postComment() {
        btnSubmit.click();
    }

    public boolean isCommentsSectionVisible() {
        return  cntComments.isCurrentlyVisible();
    }
}
