package com.automation.challenge.interfaces;

import com.automation.challenge.builders.FormSamplePageBuilder;

public interface WithEmail {
    FormSamplePageBuilder email(String theEmail);
}
