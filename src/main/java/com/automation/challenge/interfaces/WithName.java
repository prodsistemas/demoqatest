package com.automation.challenge.interfaces;

public interface WithName {
    WithEmail name(String theEmail);
}
