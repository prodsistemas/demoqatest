package com.automation.challenge.steps;

import com.automation.challenge.pages.HomePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;

public class HomeStep extends ScenarioSteps {
    @Page
    HomePage homePage;

    @Step
    public void openBrowser() {
        homePage.open();
    }

    @Step
    public void selectFooterMenuOption(String menuItem) {
        homePage.selectFooterMenuOption(menuItem);
    }
}
