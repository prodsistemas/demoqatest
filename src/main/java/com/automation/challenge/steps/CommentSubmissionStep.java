package com.automation.challenge.steps;

import com.automation.challenge.pages.CommitSubmissionPage;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;

public class CommentSubmissionStep extends ScenarioSteps {
    @Page
    CommitSubmissionPage commitSubmissionPage;

    public void verifyCommentSubmissionFailure(String message) {
        MatcherAssert.assertThat("It should have been displayed the error message. " + message, commitSubmissionPage.verifySubmission().contains(message));
    }

    public void goBack() {
        commitSubmissionPage.goBack();
    }
}
