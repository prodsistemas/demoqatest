package com.automation.challenge.steps;

import com.automation.challenge.models.FormSample;
import com.automation.challenge.pages.SamplePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;

public class SampleStep extends ScenarioSteps {
    @Page
    SamplePage samplePage;

    @Step
    public void fillAndSubmitTheComment(FormSample formSample) {
        samplePage.fillComment(formSample.getComment());
        samplePage.fillName(formSample.getName());
        samplePage.fillEmail(formSample.getEmail());
        samplePage.postComment();
    }

    public void verifyCommentPosted() {
        MatcherAssert.assertThat("It should have been posted the comment.", samplePage.isCommentsSectionVisible());
    }
}
