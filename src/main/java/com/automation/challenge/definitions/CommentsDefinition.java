package com.automation.challenge.definitions;

import com.automation.challenge.builders.FormSamplePageBuilder;
import com.automation.challenge.models.FormSample;
import com.automation.challenge.steps.CommentSubmissionStep;
import com.automation.challenge.steps.HomeStep;
import com.automation.challenge.steps.SampleStep;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.joda.time.DateTime;

public class CommentsDefinition {
    @Steps
    HomeStep homeStep;
    @Steps
    SampleStep sampleStep;
    @Steps
    CommentSubmissionStep commentSubmissionStep;
    DateTime datetime = DateTime.now();

    @Given("^I am on the sample page$")
    public void iAmOnTheSamplePage(){
        homeStep.openBrowser();
        homeStep.selectFooterMenuOption("Sample Page");
    }

    @Given("^I previously tried to send the comment '(.*)', the name '(.*)' and with the wrong email '(.*)', But it is shown an error message '(.*)'$")
    public void trySendCommentWithWrongEmailAddress(String theComment, String theName, String theWrongEmail, String message){
        FormSample formSample = FormSamplePageBuilder.comment(theComment + " "+ datetime).name(theName).email(theWrongEmail).build();
        sampleStep.fillAndSubmitTheComment(formSample);
        commentSubmissionStep.verifyCommentSubmissionFailure(message);
        commentSubmissionStep.goBack();
    }

    @When("^I try again to send the comment '(.*)', the name '(.*)' and with the correct email '(.*)'$")
    public void sendCommentWithCorrectEmailAddress(String theComment, String theName, String theCorrectEmail){
        FormSample formSample = FormSamplePageBuilder.comment(theComment + " "+ datetime).name(theName).email(theCorrectEmail).build();
        sampleStep.fillAndSubmitTheComment(formSample);
    }

    @Then("^I should post the comment successfully$")
    public void verifyCommentSentSuccessfully(){
        sampleStep.verifyCommentPosted();
    }
}
